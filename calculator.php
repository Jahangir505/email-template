<!DOCTYPE html>

<head>
	<title>Switch Case</title>

    <style>

        body{
            background: cyan;
            height: 100vh;
        }

        .testing-php{
            display: flex;
            height: 100%;
            align-items: center;
        }

        .calculator{
            width: 35%;
            margin: auto;
            border: 2px solid #ffffff;
            padding: 20px;
        }

        .evenOdd{
            width: 20%;
            margin: auto;
            border: 2px solid #ffffff;
            padding: 20px;
        }
        input[type=number], input[type=readonly] {
            width: 100%;
            padding: 12px 20px;
            margin: 8px 0;
            display: inline-block;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
        }

        input[type=submit] {
            
            background-color: #000000;
            color: white;
            padding: 14px 20px;
            margin: 8px 0;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }

        input[type=submit]:hover {
            background-color: #45a049;
        }
    </style>
</head>

<?php

// Calculator Program
$first_num = isset($_POST['first_num']) ? $_POST['first_num'] : '';
$second_num = isset($_POST['second_num']) ? $_POST['second_num'] : '';
$operator = isset($_POST['operator']) ? $_POST['operator'] : '';
$result = '';

if (is_numeric($first_num) && is_numeric($second_num)) {
    switch ($operator) {
        case "Add":
           $result = $first_num + $second_num;
            break;
        case "Subtract":
           $result = $first_num - $second_num;
            break;
        case "Multiply":
            $result = $first_num * $second_num;
            break;
        case "Divide":
            $result = $first_num / $second_num;
            break;
        case "Modulus":
            $result = $first_num % $second_num;
    }
}

// Even Odd Check Program
$number = isset($_POST['evenOdd']) ? $_POST['evenOdd'] : "";
$output = "";

if(is_numeric($number)) {
    switch((int)($number % 2)){
        case 0:
            $output = "$number is Even Number";
            break;
        case 1:
            $output = "$number is Odd Number";
    }
}

// Grade Check Program

$grade = isset($_POST['grade']) ? $_POST['grade'] : "";
$output2 = "";


if(is_numeric($grade)){
    switch($grade){
        case ($grade<= 100):
            switch((int)($grade / 10)){
                case 8:
                    $output2 = "The Grade is A+";
                    break;
                case 7:
                    $output2 = "The Grade is A";
                    break;  
                case 6:
                    $output2 = "The Grade is B";
                    break;  
                case 5:
                    $output2 = "The Grade is C";
                    break;  
                case 4:
                    $output2 = "The Grade is D";
                    break; 
                case (1 or 2 or 3):
                    $output2 = "Fail!"; 
                    break;
                case 0:
                    $output2 = "Fail!";
            }
            break;
        case ($grade > 100):
            $output2 = "$grade is invalid number";
    }
}

?>

<body>
    <div class="testing-php">
        <div class="calculator" id="page-wrap">
            <h1 style="font-size: 18px;">PHP - Calculator Program Using Switch</h1>
            <form action="" method="post" id="quiz-form">
                    <p>
                    <input type="number" name="first_num" id="first_num" placeholder="Enter first number..." required="required" value="<?php echo $first_num; ?>" /> 
                    </p>
                    <p>
                    <input type="number" name="second_num" id="second_num" placeholder="Enter second number..." required="required" value="<?php echo $second_num; ?>" /> 
                    </p>
                    <p>
                    <b>Output:</b> <input style="padding: 12px 20px;
                    margin: 8px 0; display: inline-block;" readonly="readonly" name="result" value="<?php echo $result; ?>"> 
                    </p>
                    <input type="submit" name="operator" value="Add" />
                    <input type="submit" name="operator" value="Subtract" />
                    <input type="submit" name="operator" value="Multiply" />
                    <input type="submit" name="operator" value="Divide" />
                    <input type="submit" name="operator" value="Modulus" />
            </form>
        </div>
        <div class="evenOdd" id="page-wrap">
            <h1 style="font-size: 18px;">PHP - Even Odd Using Switch</h1>
            <form action="" method="post" id="quiz-form">
                    <p>
                    <input type="number" name="evenOdd" id="evenOdd" placeholder="Enter a number..." required="required" value="<?php echo $number; ?>" /> 
                    </p>
                    
                    <p>
                    <b>Output:</b> <input style="padding: 12px 20px;
                    margin: 8px 0; display: inline-block;" readonly="readonly"  value="<?php echo $output; ?>"> 
                    </p>
                    <input type="submit" name="operator" value="Submit" />
            </form>
        </div>
        <div class="evenOdd" id="page-wrap">
            <h1 style="font-size: 18px;">PHP - Grade System Using Switch</h1>
            <form action="" method="post" id="quiz-form">
                    <p>
                    <input type="number" name="grade" id="grade" placeholder="Enter a number..." required="required" value="<?php echo $grade; ?>" /> 
                    </p>
                    
                    <p>
                    <b>Output:</b> <input style="padding: 12px 20px;
                    margin: 8px 0; display: inline-block;" readonly="readonly"  value="<?php echo $output2; ?>"> 
                    </p>
                    <input type="submit" name="operator" value="Submit" />
            </form>
        </div>
    </div>
</body>
</html>