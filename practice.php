<?php

$color = array('white', 'green', 'red', 'blue', 'black');

echo "The memory of that scene for me is like a frame of film forever frozen at that moment: the $color[2] carpet, the $color[1] lawn, the $color[0] house, the leaden sky. The new president and his first lady. - Richard M. Nixon";


// Insert new item in Array

$item_list = [1,2,3,4,5];

array_push($item_list, "$");
echo "<pre>";
print_r($item_list) ;
echo "</pre>";


// Sorting associative array

$age=array("Sophia"=>"31","Jacob"=>"41","William"=>"39","Ramesh"=>"40");

// ascending order sort by value

    asort($age);

    echo "<pre>";
    print_r($age) ;
    echo "</pre>";

// ascending order sort by key
    ksort($age);
    
    echo "<pre>";
    print_r($age) ;
    echo "</pre>";

// descending order sorting by Value
    arsort($age);
        
    echo "<pre>";
    print_r($age) ;
    echo "</pre>";

// descending order sorting by Key
    krsort($age);
            
    echo "<pre>";
    print_r($age) ;
    echo "</pre>";


    // Display Average Temperature
    
    $temperatures = [78, 60, 62, 68, 71, 68, 73, 85, 66, 64, 76, 63, 81, 76, 73,
    68, 72, 73, 75, 65, 74, 63, 67, 65, 64, 68, 73, 75, 79, 73];

    $total_tempt = 0;
    $array_length = count($temperatures);

    foreach ($temperatures as $tempt) {
        $total_tempt += $tempt;
    }

    $avg_high_tempt = $total_tempt / $array_length;

    echo "Average Temperature is : ".$avg_high_tempt;
    echo "<br>";
    sort($temperatures);
    echo " List of seven lowest temperatures : ";

    for ($i=0; $i<7; $i++) {
        echo $temperatures[$i]. ", ";
    }
    echo "<br>";
    echo "List of seven highest temperatures : ";
    for ($i=($array_length-7); $i< ($array_length); $i++) {
        echo $temperatures[$i].", ";
    }
    echo "<br>";
    // 5.	Write a PHP script which displays all the numbers between 200 and 250 that are divisible by 4

    for ($i=200; $i<=250; $i+=4) {
        echo "$i, ";
    }

    echo "<br>";

    $first_array = array('c1' => 'Red', 'c2' => 'Green', 'c3' => 'White', 'c4' => 'Black');
    $second_array = array('c2', 'c4');
    print_r(array_intersect_key($first_array, array_flip($second_array)));
    echo "<br>";

    // hyphen Add

    for ($i=1; $i<=10; $i++) {
        if ($i< 10) {
            echo "$i-";
        } else {
            echo "$i"."\n";
        }
    }

    echo "<br>";

    // Specific Pattern

    $num=5;
    for ($i=1; $i<=$num; $i++) {
        for ($j=1; $j<=$i; $j++) {
            echo ' * ';
        }
        echo '<br>';
    }
    for ($i=$num; $i>=1; $i--) {
        for ($j=1; $j<=$i; $j++) {
            echo ' * ';
        }
        echo '<br>';
    }

    echo "<br>";

    // Write a program which will count the "l" characters in the text "Hello Pondits. How are you all?"

    $text="Hello Pondits. How are you all?";
    $find="l";
    $count="0";
    for ($x="0"; $x< strlen($text); $x++) {
        if (substr($text, $x, 1)==$find) {
            $count=$count+1;
        }
    }
    echo $count."<br>";

echo "<br>";

?>


<table align="left" border="1" cellpadding="3" cellspacing="0">
<?php
    for ($i=1;$i<=6;$i++) {
        echo "<tr>";
        for ($j=1;$j<=5;$j++) {
            echo "<td>$i * $j = ".$i*$j."</td>";
        }
        echo "</tr>";
    }
?>
</table>

<table border="1" style='border-collapse: collapse'>

    <?php
    
        for ($row=1; $row <= 10; $row++) {
            echo "<tr> <br>";
            for ($col=1; $col <= 10; $col++) {
                $p = $col * $row;
                echo "<td>$p</td>";
            }
            echo "</tr>";
        }
   ?>
</table> 
