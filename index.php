<?php 


$template = <<<EMAIL_TEMPLATE

<body class="pc-fb-font" bgcolor="#f4f4f4" style="background-color: #f4f4f4; font-family: 'Fira Sans', Helvetica, Arial, sans-serif; font-size: 16px; width: 100% !important; Margin: 0 !important; padding: 0; line-height: 1.5; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%">
  <table style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; margin: 20px 0;" width="100%" border="0" cellpadding="0" cellspacing="0">
    <tbody>
      <tr>
        <td style="padding: 0; vertical-align: top;" align="center" valign="top">
          <table style="max-width: 650px; width: 100%; text-align: center; background-color: #ffffff;">
            <tr style="height: 60px"></tr>
            <tr>
                <td style="vertical-align: top; text-align: center; font-family: 'Fira Sans', Helvetica, Arial, sans-serif; font-weight: 500; font-size: 14px; color: #40BE65;">Introducing</td>
            </tr>
            <tr>
                <td class="pc-header-cta-title pc-fb-font" style="vertical-align: top; padding: 12px 0 0; text-align: center; font-family: 'Fira Sans', Helvetica, Arial, sans-serif; font-size: 36px; font-weight: 800; line-height: 1.28; letter-spacing: -0.6px; color: #1B1B1B;" valign="top" align="center"> Tousled food truck <br>polaroid, salvia. </td>
            </tr>
            <tr>
                <td style="vertical-align: top; padding: 30px 0 0 0; text-align: center;" valign="top" align="center"> <img src="images/header-4-image-1.jpg" width="346" height="277" alt="" style="border: 0; line-height: 100%; outline: 0; -ms-interpolation-mode: bicubic; display: block; Margin: 0 auto; max-width: 100%; height: auto; font-family: 'Fira Sans', Helvetica, Arial, sans-serif; font-size: 14px; color: #1B1B1B;"> 
                </td>
            </tr>
            <tr>
                <td class="pc-header-cta-text pc-fb-font" style="vertical-align: top; padding: 30px 0 0 0; text-align: center; font-family: 'Fira Sans', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 300; line-height: 1.56; letter-spacing: -0.2px; color: #9B9B9B;" valign="top" align="center"> Co-ordinate campaigns and product launches, <br>with improved overall communication.
                </td>
            </tr>
            <tr style="height: 24px;"></tr>
            <tr>
                <td style="vertical-align: top; border-radius: 8px; text-align: center;  " valign="top" align="center"> <a href="#" style="line-height: 1.5; text-decoration: none; margin: 0; padding: 13px 17px; white-space: nowrap; border-radius: 8px; font-weight: 500;  font-family: 'Fira Sans', Helvetica, Arial, sans-serif; font-size: 16px; cursor: pointer; background-color: #1595E7; color: #ffffff; border: 1px solid #1595E7;">Download manual</a> 
                </td>
            </tr>
            <tr style="height: 100px;"></tr>
            <tr>
                <td class="pc-fb-font" style="vertical-align: top; text-align: center; font-family: 'Fira Sans', Helvetica, Arial, sans-serif; font-size: 24px; font-weight: 700; line-height: 1.42; letter-spacing: -0.4px; color: #151515; padding: 0 20px;" valign="top" align="center">Features.
                </td>
            </tr>
            <tr>
                <td class="pc-fb-font" style="vertical-align: top; text-align: center; font-family: 'Fira Sans', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 300; line-height: 1.56; letter-spacing: -0.2px; color: #9B9B9B; padding: 0 20px;" valign="top" align="center"> Co-ordinate campaigns and product launches, <br>with improved overall communication. 
                </td>
            </tr>
            <tr>
                <td>
                    <table style="padding: 40px 20px 35px;">
                        <tr>
                            <td style="padding:20px;">
                                <table>
                                    <tr>
                                        <td style="vertical-align: top; text-align: center;" valign="top" align="center"> <img src="images/feature-5-image-1.jpg" width="74" height="74" alt="" style="border: 0; line-height: 100%; outline: 0; -ms-interpolation-mode: bicubic; display: block; font-family: 'Fira Sans', Helvetica, Arial, sans-serif; font-size: 14px; color: #1B1B1B; text-align: center; Margin: 0 auto;"> </td>
                                    </tr>
                                    <tr>
                                        <td class="pc-fb-font" style="vertical-align: top; font-family: 'Fira Sans', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 500; line-height: 1.33; letter-spacing: -0.2px; color: #1B1B1B; text-align: center;" valign="top" align="center">Online</td>
                                    </tr>
                                    <tr>
                                        <td class="pc-fb-font" style="vertical-align: top; font-family: 'Fira Sans', Helvetica, Arial, sans-serif; font-size: 14px; font-weight: 300; line-height: 1.43; letter-spacing: -0.2px; color: #9B9B9B; text-align: center;" valign="top" align="center">Bushwick meh Blue Bottle pork belly mustache sk.</td>
                                    </tr>
                                </table>
                            </td>
                            <td style="padding:20px;">
                                <table>
                                    <tr>
                                        <td style="vertical-align: top; text-align: center;" valign="top" align="center"> <img src="images/feature-5-image-2.jpg" width="74" height="74" alt="" style="border: 0; line-height: 100%; outline: 0; -ms-interpolation-mode: bicubic; display: block; font-family: 'Fira Sans', Helvetica, Arial, sans-serif; font-size: 14px; color: #1B1B1B; text-align: center; Margin: 0 auto;"> </td>
                                    </tr>
                                    <tr>
                                        <td class="pc-fb-font" style="vertical-align: top; font-family: 'Fira Sans', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 500; line-height: 1.33; letter-spacing: -0.2px; color: #1B1B1B; text-align: center;" valign="top" align="center">Diverse</td>
                                    </tr>
                                    <tr>
                                        <td class="pc-fb-font" style="vertical-align: top; font-family: 'Fira Sans', Helvetica, Arial, sans-serif; font-size: 14px; font-weight: 300; line-height: 1.43; letter-spacing: -0.2px; color: #9B9B9B; text-align: center;" valign="top" align="center">Keytar McSweeney's Williamsburg, readymade legg.</td>
                                    </tr>
                                </table>
                            </td>
                            <td style="padding:20px;">
                                <table>
                                    <tr>
                                        <td style="vertical-align: top; text-align: center;" valign="top" align="center"> <img src="images/feature-5-image-3.jpg" width="74" height="74" alt="" style="border: 0; line-height: 100%; outline: 0; -ms-interpolation-mode: bicubic; display: block; font-family: 'Fira Sans', Helvetica, Arial, sans-serif; font-size: 14px; color: #1B1B1B; text-align: center; Margin: 0 auto;"> </td>
                                    </tr>
                                    <tr>
                                        <td class="pc-fb-font" style="vertical-align: top; font-family: 'Fira Sans', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 500; line-height: 1.33; letter-spacing: -0.2px; color: #1B1B1B; text-align: center;" valign="top" align="center">Interactive</td>
                                    </tr>
                                    <tr>
                                        <td class="pc-fb-font" style="vertical-align: top; font-family: 'Fira Sans', Helvetica, Arial, sans-serif; font-size: 14px; font-weight: 300; line-height: 1.43; letter-spacing: -0.2px; color: #9B9B9B; text-align: center;" valign="top" align="center">Hella narwhal Cosby sweater McSweeney's, salvia.</td>
                                    </tr>
                                    
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="vertical-align: top; border-radius: 8px; text-align: center;  valign="top"  align="center"> <a href="#" style="line-height: 1.5; text-decoration: none; margin: 0; padding: 13px 17px; white-space: nowrap; border-radius: 8px; font-weight: 500; display: inline-block; font-family: 'Fira Sans', Helvetica, Arial, sans-serif; font-size: 16px; cursor: pointer; background-color: #1595E7; color: #ffffff; border: 1px solid #1595E7;">Learn More</a> 
                </td>
            </tr>
            <tr style="height: 60px"></tr>
            <tr>
                <td class="pc-fb-font" style="vertical-align: top; padding: 0 20px; text-align: center; font-family: 'Fira Sans', Helvetica, Arial, sans-serif; font-size: 24px; font-weight: 700; line-height: 1.42; letter-spacing: -0.4px; color: #151515;" valign="top" align="center">Our clients.
                </td>
            </tr>
            <tr>
                <td class="pc-fb-font" style="vertical-align: top; padding: 0 20px; text-align: center; line-height: 1.56; font-family: 'Fira Sans', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 300; color: #9B9B9B; letter-spacing: -0.2px;" valign="top" align="center">Co-ordinate campaigns and product launches.</td>
            </tr>
            <tr>
                <td>
                    <table style="width: 100%;">
                        <tr>
                            <td style="vertical-align: top; padding: 20px; text-align: center;" valign="top" align="center"> 
                                <img src="images/content-4-image-1.jpg" width="67" height="19" alt="" style="border: 0; line-height: 100%; outline: 0; -ms-interpolation-mode: bicubic; text-decoration: none; display: block; color: #151515; max-width: 100%; height: auto; font-size: 14px; font-family: 'Fira Sans', Helvetica, Arial, sans-serif; Margin: 0 auto;"> 
                            </td>
                            <td style="vertical-align: top; padding: 20px; text-align: center;" valign="top" align="center"> 
                                <img src="images/content-4-image-2.jpg" width="67" height="19" alt="" style="border: 0; line-height: 100%; outline: 0; -ms-interpolation-mode: bicubic; text-decoration: none; display: block; color: #151515; max-width: 100%; height: auto; font-size: 14px; font-family: 'Fira Sans', Helvetica, Arial, sans-serif; Margin: 0 auto;"> 
                            </td>
                            <td style="vertical-align: top; padding: 20px; text-align: center;" valign="top" align="center"> 
                                <img src="images/content-4-image-3.jpg" width="40" height="19" alt="" style="border: 0; line-height: 100%; outline: 0; -ms-interpolation-mode: bicubic; text-decoration: none; display: block; color: #151515; max-width: 100%; height: auto; font-size: 14px; font-family: 'Fira Sans', Helvetica, Arial, sans-serif; Margin: 0 auto;"> 
                            </td>
                            <td style="vertical-align: top; padding: 20px; text-align: center;" valign="top" align="center"> 
                                <img src="images/content-4-image-4.jpg" width="67" height="19" alt="" style="border: 0; line-height: 100%; outline: 0; -ms-interpolation-mode: bicubic; text-decoration: none; display: block; color: #151515; max-width: 100%; height: auto; font-size: 14px; font-family: 'Fira Sans', Helvetica, Arial, sans-serif; Margin: 0 auto;"> 
                            </td>
                            
                        </tr>
                        <tr>
                            <td style="vertical-align: top; padding: 20px; text-align: center;" valign="top" align="center"> 
                                <img src="images/content-4-image-5.jpg" width="67" height="19" alt="" style="border: 0; line-height: 100%; outline: 0; -ms-interpolation-mode: bicubic; text-decoration: none; display: block; color: #151515; max-width: 100%; height: auto; font-size: 14px; font-family: 'Fira Sans', Helvetica, Arial, sans-serif; Margin: 0 auto;"> 
                            </td>
                            <td style="vertical-align: top; padding: 20px; text-align: center;" valign="top" align="center"> 
                                <img src="images/content-4-image-6.jpg" width="67" height="19" alt="" style="border: 0; line-height: 100%; outline: 0; -ms-interpolation-mode: bicubic; text-decoration: none; display: block; color: #151515; max-width: 100%; height: auto; font-size: 14px; font-family: 'Fira Sans', Helvetica, Arial, sans-serif; Margin: 0 auto;"> 
                            </td>
                            <td style="vertical-align: top; padding: 20px; text-align: center;" valign="top" align="center"> 
                                <img src="images/content-4-image-7.jpg" width="40" height="19" alt="" style="border: 0; line-height: 100%; outline: 0; -ms-interpolation-mode: bicubic; text-decoration: none; display: block; color: #151515; max-width: 100%; height: auto; font-size: 14px; font-family: 'Fira Sans', Helvetica, Arial, sans-serif; Margin: 0 auto;"> 
                            </td>
                            <td style="vertical-align: top; padding: 20px; text-align: center;" valign="top" align="center"> 
                                <img src="images/content-4-image-8.jpg" width="67" height="19" alt="" style="border: 0; line-height: 100%; outline: 0; -ms-interpolation-mode: bicubic; text-decoration: none; display: block; color: #151515; max-width: 100%; height: auto; font-size: 14px; font-family: 'Fira Sans', Helvetica, Arial, sans-serif; Margin: 0 auto;"/> 
                            </td>
                            
                        </tr>
                    </table>
                </td>
            </tr>
            <tr style="height:60px;"></tr>
            <tr>
                <td class="pc-cta-title pc-fb-font" style="vertical-align: top; font-family: 'Fira Sans', Helvetica, Arial, sans-serif; color: #151515; font-size: 24px; font-weight: 700; line-height: 1.42; letter-spacing: -0.4px; text-align: center;" valign="top" align="center">App is available for iOS and Android.</td>
            </tr>
            <tr>
                <td class="pc-cta-text pc-fb-font" style="vertical-align: top; font-family: 'Fira Sans', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 300; line-height: 1.56; color: #9B9B9B; text-align: center;" valign="top" align="center"> Co-ordinate campaigns and product launches, <br>with improved overall communication. </td>
            </tr>
            <tr>
                <td>
                    <table style="width: 50%; margin: auto; text-align: center;">
                        <tr>
                            <td style="vertical-align: top; padding: 8px; text-align: center;" valign="top" align="center"> 
                                <a href="#" style="text-decoration: none;">
                                    <img src="images/button-app-store-dark.png" width="127" height="52" alt="" style="border: 0; line-height: 100%; outline: 0; -ms-interpolation-mode: bicubic; height: auto; font-family: 'Fira Sans', Helvetica, Arial, sans-serif; font-size: 14px; color: #ffffff; max-width: 100%; display: block;">
                                </a> 
                            </td>
                            <td style="vertical-align: top; padding: 8px; text-align: center;" valign="top" align="center"> 
                                <a href="#" style="text-decoration: none;">
                                    <img src="images/button-app-store-dark.png" width="127" height="52" alt="" style="border: 0; line-height: 100%; outline: 0; -ms-interpolation-mode: bicubic; height: auto; font-family: 'Fira Sans', Helvetica, Arial, sans-serif; font-size: 14px; color: #ffffff; max-width: 100%; display: block;">
                                </a> 
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr style="height: 50px;"></tr>
            <tr>
                <td>
                    <table style=" width: 100%;">
                        <tr style="display:flex; justify-content: space-between; padding: 0 40px;">
                            <td class="pc-footer-text-s2 pc-mobile-text-centered pc-fb-font" style="vertical-align: top; line-height: 1.43; letter-spacing: -0.2px; font-family: 'Fira Sans', Helvetica, Arial, sans-serif; font-size: 14px; color: #9B9B9B; padding: 10px 0;" valign="top"> 
                                <a style="text-decoration: none; color: #9B9B9B; cursor: text;">King street, 2901 Marmara road,
                                <br>Newyork, WA 98122-1090</a> 
                            </td>

                            <td class="pc-mobile-text-centered" style="vertical-align: top; line-height: 1.3; font-size: 20px; font-family: 'Fira Sans', Helvetica, Arial, sans-serif; padding: 4px 0 10px; text-align: left;" valign="top" align="left"> 
                                <a href="#" style="text-decoration: none;">
                                    <img src="images/facebook-dark.png" width="15" height="15" alt="" style="border: 0; line-height: 100%; outline: 0; -ms-interpolation-mode: bicubic; font-size: 14px; color: #151515;">
                                </a> <span>&nbsp;&nbsp;</span> 
                                <a href="#" style="text-decoration: none;">
                                    <img src="images/twitter-dark.png" width="16" height="14" alt="" style="border: 0; line-height: 100%; outline: 0; -ms-interpolation-mode: bicubic; font-size: 14px; color: #151515;">
                                </a> <span>&nbsp;&nbsp;</span> 
                                <a href="#" style="text-decoration: none;">
                                    <img src="images/google-plus-dark.png" width="22" height="15" alt="" style="border: 0; line-height: 100%; outline: 0; -ms-interpolation-mode: bicubic; font-size: 14px; color: #151515;">
                                </a> <span>&nbsp;&nbsp;</span> 
                                <a href="#" style="text-decoration: none;">
                                    <img src="images/instagram-dark.png" width="16" height="15" alt="" style="border: 0; line-height: 100%; outline: 0; -ms-interpolation-mode: bicubic; font-size: 14px; color: #151515;">
                                </a> 
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr style="height: 60px;"></tr>
            <tr>
                <td class="pc-fb-font" style="vertical-align: top; text-align: center; line-height: 1.43; font-family: 'Fira Sans', Helvetica, Arial, sans-serif; font-size: 14px; color: #9B9B9B;" valign="top" align="center"> 
                    <a href="#" style="text-decoration: none; color: #1595E7;">Manage Preferences</a> <span>&nbsp;&nbsp;</span> <a href="#" style="text-decoration: none; color: #1595E7;">Unsubscribe</a>
                 </td> 
            </tr>
            <tr style="height: 30px;"></tr>
          </table>
        </td>
      </tr>
    </tbody>
  </table>
</body>



EMAIL_TEMPLATE;

echo $template;